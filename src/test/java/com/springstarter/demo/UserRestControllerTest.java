package com.springstarter.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.springstarter.demo.model.User;
import com.springstarter.demo.repository.UserRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
class UserRestControllerTest {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetUsers() {
		User user1 = new User("Baron","Loki","01.01.2019","bd ecureuils","Mandelieu", "06210","0609637613");
		User user2 = new User("Baron","Timy","01.05.2015","bd ecureuils","Mandelieu", "06210","0609637613");		
		entityManager.persist(user1);
		entityManager.persist(user2);
		
		List<User> allUsersFromDb = userRepository.findAll();
		assertThat(allUsersFromDb.size()).isEqualTo(2);
		
	}

	@Test
	void testGetUserById() {
		User user = new User("Baron","Loki","01.01.2019","bd ecureuils","Mandelieu", "06210","0609637613");
		User userSavedInBd = entityManager.persist(user);
		User userFromDb = userRepository.getOne(userSavedInBd.getId());
		assertEquals(userSavedInBd,userFromDb);
		assertThat(userFromDb.equals(userSavedInBd));
		
	}

	@Test
	void testSave() {
		User user = new User("Baron","Loki","01.01.2019","bd ecureuils","Mandelieu", "06210","0609637613");
		User userSavedInBd = entityManager.persist(user);
		User userFromDb = userRepository.getOne(userSavedInBd.getId());
		assertEquals(userSavedInBd,userFromDb);
		assertThat(userFromDb.equals(userSavedInBd));
		
	}

	@Test
	void testUpdateUser() {
		User user = new User("Baron","Loki","01.01.2019","bd ecureuils","Mandelieu", "06210","0609637613");
		entityManager.persist(user);
		User userGetFromDb = userRepository.getOne(user.getId());
		userGetFromDb.setNom("Baroni");
		entityManager.persist(userGetFromDb);
		User userGetFromDb1New= userRepository.getOne(user.getId()); 
		assertThat(userGetFromDb1New.getNom().equals("Baroni"));
		
	}

	@Test
	void testDeleteUser() {
		User user1 = new User("Baron","Loki","01.01.2019","bd ecureuils","Mandelieu", "06210","0609637613");
		User user2 = new User("Baron","Timy","01.05.2015","bd ecureuils","Mandelieu", "06210","0609637613");
		User userSavedInDb = entityManager.persist(user1);
		entityManager.persist(user2);
		entityManager.remove(userSavedInDb);
		
		
		List<User> users = userRepository.findAll();
		assertThat(users.size()).isEqualTo(1);
	}

}
