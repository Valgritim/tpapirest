package com.springstarter.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TPapiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TPapiRestApplication.class, args);
	}

}
