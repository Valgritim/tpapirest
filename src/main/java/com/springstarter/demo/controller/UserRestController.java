package com.springstarter.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springstarter.demo.exceptions.ResourceNotFoundException;
import com.springstarter.demo.model.User;
import com.springstarter.demo.repository.UserRepository;

@RestController
public class UserRestController {
	
	@Autowired
	private UserRepository userRepository;
	
	private EntityManager entityManager;
	
	@GetMapping("/users")
	public List<User> getUsers(){
		return userRepository.findAll();		
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable Long id){
		User user = userRepository.findById(id)
					.orElseThrow(()-> new ResourceNotFoundException("User not found::" + id));
		return ResponseEntity.ok().body(user);		
	}
	
	@PostMapping("/users")
	public User save(@RequestBody User u) {
		return userRepository.save(u);
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateUser(@RequestBody User u,
			@PathVariable(value = "id") Long id) throws ResourceNotFoundException{
			User user = userRepository.findById(id)
					.orElseThrow(()-> new ResourceNotFoundException("User not found::" + id));
			user.setNom(u.getNom());
			user.setPrenom(u.getPrenom());
			user.setDateAnniversaire(u.getDateAnniversaire());
			user.setRue(u.getRue());
			user.setVille(u.getVille());
			user.setCodePostal(u.getCodePostal());
			user.setTelephone(u.getTelephone());			
			final User updatedUser = userRepository.save(user);
			return  ResponseEntity.ok(updatedUser);			
	}
	
	@DeleteMapping("/users/{id}")
	public Map<String, Boolean> deleteUser(@RequestBody User u,
			@PathVariable(value="id") Long id) throws ResourceNotFoundException{
		User user = userRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("User not found::" + id));
		userRepository.delete(user);
		Map < String, Boolean > response = new HashMap < > ();
        response.put("deleted", Boolean.TRUE);
        return response;
		
	}
}
