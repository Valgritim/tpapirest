package com.springstarter.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springstarter.demo.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
